#!/bin/bash
while true; do
while ! inotifywait -e close_write,move_self $1.tex; do true; done
sleep 0.5
if pdflatex --shell-escape $1.tex < /dev/null ; then
	evince $1.pdf
fi
done

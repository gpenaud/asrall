#! /bin/bash

timestamp=$(date +"%s")

i1=1
i2=10
i3=60

l1=10
l2=60
l3=300

quit=true

while ($quit -ne false)
do
  if ($(($timestamp+$d1)) -le $(($timestamp+$l1))); then
    echo "execute d1"
  fi
  if ($(($timestamp+$d2)) -le $(($timestamp+$l2))); then
    echo "execute d2"
  fi
  if ($(($timestamp+$d3)) -le $(($timestamp+$l3))); then
    echo "execute d3"
  fi
done

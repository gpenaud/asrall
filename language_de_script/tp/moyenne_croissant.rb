#!/usr/bin/ruby

somme = 0
nombre_notes = 4
tab = []

nombre_notes.times do
  puts "Saisir une note:"
  tab << gets.strip.to_f
end

i = 1
tab.sort.each do |note| 
  puts "#{i}ème note: #{note}"
  i = i + 1
end

puts "La moyenne est égale à #{tab.inject{ |sum, el| sum + el}.to_f / nombre_notes}"



<?php 
session_start();

// récupère $pdo
include('config.php');

// Expiration positionnée à 15 jours
$expiration = 60 * 60 * 24 * 15;

// Calcul du nouveau meilleur score
if (isset($_COOKIE['meilleurscore']))
  if ($_COOKIE['meilleurscore'] < $_SESSION['scoregen'])
    $meilleurScore = $_SESSION['scoregen'];
  else
    $meilleurScore = $_COOKIE['meilleurscore'];
else
  $meilleurScore = $_SESSION['scoregen'];

// Positionnement le meilleur score
setcookie('meilleurscore', $meilleurScore, time() + $expiration);

$stmt = $pdo->prepare("
    INSERT INTO scores 
        (nom, score, date_partie_jouee) 
    VALUES 
        (:nom, :score, NOW())
"); 

$stmt->bindParam(':nom', $_SESSION['nom']);
$stmt->bindParam(':score', $_SESSION['scoregen']);

if (!$stmt->execute()) {
    // error
}

// On supprime la session et on file à l'accueil
session_destroy();
header('Location: index.php');
 ?>

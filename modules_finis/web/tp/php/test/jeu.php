<?php

include("tools.php");

function gestionTour()
{
  enteteTitreHTML("Jeu de dés !");
  
  $de1 = rand(1, 6);
  $de2 = rand(1, 6);
  $score_courant = $de1 + $de2;
  
  echo "Partie de " . $_SESSION["nom"] . "<br/>";
  echo "Tour no " . $_SESSION["notour"] . "<br/>";
  echo "Valeur dé 1 : " . $de1 . "<br/>";
  echo "Valeur dé 2 : " . $de2 . "<br/>";
  echo "Total des dés : " . $score_courant . "<br/>";
  echo "Score avant le lancer : " . $_SESSION["scoregen"] . "<br/>";
  
  if ($score_courant == 7)
    $_SESSION["scoregen"] += 10;
  
  echo "Score après le lancer : " . $_SESSION["scoregen"] . "<br/>";
  
  if ($_SESSION["notour"] == 10)
    echo "<a href=\"fin.php\">Fin de partie</a>";
  else
    echo "<a href=\"jeu.php\">Jet suivant</a>";
}

/* PROGRAMME PRINCIPAL */

// Vérification : est-ce qu'on provient
// du formulaire ? Et est-ce que le champ
// nom a été rempli -> redirection vers
// l'accueil sinon

// On vérifie ensuite si on provient
// de la page elle-même... 

if (isset($_POST['nom']))
  $nom = trim($_POST['nom']);

if (empty($nom)) {
  if (!isset($_SESSION['nom']))
    header('Location: index.php');
  else {
    $_SESSION["notour"]++;
    gestionTour();
  }
}
else {

  // Initialisation de quelques variables
  $_SESSION["nom"] = $nom;
  $_SESSION["scoregen"] = 0;
  $_SESSION["notour"] = 1;

  gestionTour();
}

?>

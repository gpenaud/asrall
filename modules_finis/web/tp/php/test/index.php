<?php

include("tools.php");
include('config.php');

define("MAX_SCORE",     30);

enteteTitreHTML("Jeu de dés !");

$sql = "SELECT COUNT(1) FROM scores";
$nb_scores = $pdo->query($sql)->fetch()[0];


if (isset($_GET['offset'])) {
    $_SESSION['offset'] = $_GET['offset'];
} else {
    $_SESSION['offset'] = 0;
}


$left_offset = $right_offset = '';

if (isset($_SESSION['offset'])) {

    if ($_SESSION['offset'] > 0)
        $left_offset = '<a href="index.php?offset=' . ($_SESSION['offset'] - PER_PAGE) . '"><<';
    

    if ($_SESSION['offset'] < ($nb_scores - PER_PAGE))
        $right_offset = '<a href="index.php?offset=' . ($_SESSION['offset'] + PER_PAGE) . '">>>';
}

// gère la limitation à 30
while (($_SESSION['offset'] + PER_PAGE) > 30) {
    $_SESSION['offset']--;
}

$sql = "SELECT nom, score, date_partie_jouee FROM scores ORDER BY score DESC, nom ASC LIMIT :per_page OFFSET :offset";

$stmt = $pdo->prepare($sql);

# les constantes ne 
$per_page = PER_PAGE;
$offset = (int) $_SESSION['offset'];

$stmt->bindParam(':per_page', $per_page, PDO::PARAM_INT);
$stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
$stmt->execute();

$scores = $stmt->fetchAll();

?>

<form method="post" action="jeu.php">
  Votre nom : <input type="text" name="nom" />
  <br />
  <input type="submit" value="C'est parti !"/>
</form>

<h1>Meilleur score</h1>
<?php 
if (isset($_COOKIE["meilleurscore"]))
  echo $_COOKIE["meilleurscore"] . " points";
else
  echo "Pas de meilleur score pour le moment...";
?>

<form method="post" action="raz.php">
  <input type="submit" value="Effacer les scores" />
</form>

<ul>
<?php
    foreach ($scores as $score) {
        echo '<li>';
        echo '<div style="display:inline-block;">' . $score["nom"] . ' ::  </div>';
        echo '<div style="display:inline-block;">' . $score["score"] . ' points ::  </div>';
        echo '<div style="display:inline-block;">' . $score["date_partie_jouee"] . '</div>';
        echo '</li>';
    }
?>
</ul>

<div>
    <?php
    echo '<span>' . $left_offset . '</span>';
    echo '<span>' . $right_offset . '</span>';
    ?>
</div>

<?php
finHTML();
?>

<?php

$matches = array();
$file = file_get_contents("apache.log");

$nb_request = preg_match_all('/.+/', $file);
preg_match_all('/([\d]+)(\n|$)/', $file, $matches);
$data_quantity = array_sum($matches[1]);

preg_match_all('/(\n|^)([\d\.]+).*GET/', $file, $matches);
$ip_addresses = array_unique($matches[2]);

preg_match_all('/(GET|POST)\s(\/(\d+|\w+)\/[\w+\.]+)/', $file, $matches);
$files = $matches[2];

$file_occurences = array();

foreach ($files as $file) {
	if (isset($file_occurences[$file])) {
		$file_occurences[$file] = ++$file_occurences[$file];
	} else {
		$file_occurences[$file] = 1;
	}
}

arsort($file_occurences);

echo '<head>';
if (true) {
	echo '<meta charset="utf-8"/>';
	echo '<title>Statistiques apache</title>';

	echo '<style type="text/css">';
	if (true) {
		echo 'body {font-family: "Trebuchet MS", Helvetica, sans-serif; font-size:1em; width:100%; margin:auto; max-width:965px;}';
		echo 'h1 {text-decoration:underline; margin-bottom:30px; text-align:center}';
		echo 'ul {margin-bottom:15px;}';
		echo 'li {list-style-type:none; font-size: 0.9em}';
	}
	echo '</style>';
}
echo '</head>';
echo '<body>';
if (true) {
	echo '<h1>Log data</h1>';
	echo '<p>nombre de requêtes traitées: ' . $nb_request . '</p>';
	echo '<p>quantité totale de données transférées: ' . $data_quantity . '</p>';

	echo '<div>liste des machines qui ont demandé des documents: </div>';
	echo '<ul>';
	foreach ($ip_addresses as $ip_address) {
		echo '<li>' . $ip_address . '</li>';
	}
	echo '</ul>';

	echo '<div>liste des 5 fichiers les plus demandés avec le nombre de demandes: </div>';
	echo '<ul>';
	foreach ($file_occurences as $filename => $file_occurence) {
		echo '<li>' . $filename . ': ' . $file_occurence . '</li>';
	}
	echo '</ul>';
}
echo '</body>';
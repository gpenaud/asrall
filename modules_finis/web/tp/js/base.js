/* absolutize the value set in input */
function absolutize(element) {
	absolutized_value = Math.abs(element.value);

	alert(absolutized_value);
}

/* process the mystery number game*/
function get_mystery_number() {
	window.alert("bienvenue dans le jeu du nombre mystère");
	
	found = false;
	random = Math.floor((Math.random() * 100) + 1); 

	while(!found) {
		user_value = prompt("Saisissez une valeur: ");
		if (random > user_value) {
			window.alert("Le bon nombre est plus grand ");
		} else if (random < user_value) {
			window.alert("Le bon nombre est plus petit ");
		} else {
			found = true;
			window.alert("bravo, vous avez trouvé !");
		}
	}
}

/* absolutize the value set in input */
function sum_first_number() {
	number = prompt("Saisissez un nombre pour voir s'il est premier: ");

	is_first = true; 

	if (number % 2 == 0) {
		is_first = false;
	}

	for (i = 3; i <= Math.sqrt(number); i += 2) {
		if (number % i == 0) {
			is_first = false;
		}
	}

	if (is_first) {
		window.alert("Ce nombre est premier ");
	} else {
		window.alert("Ce nombre n'est pas premier ");
	}
}

function formulary_check() {
	var form_submit = true;
	var password = null;
	var focused = null;

	var fields = ["last_name", "first_name", "birthdate", "password", "confirm_password"];

	fields.forEach(function(field) {
		field_name = field;
		field = document.getElementById(field);

		// reset des styles
		field.className = "";

		// comparaison des champs password
		if (field_name == "password") {
			password = field;
		} else if (field_name == "confirm_password") {			
			if (password.value != field.value) {
				// ajout de l'erreur à password et confirm_password
				password.className = password.className + " password-error";
				field.className = field.className + " password-error";
				// pas d'envoi de formulaire
				form_submit = false;
			} 
		}

		if (!field.value) {
			if (focused == null) {
				focused = field;
			}
			field.className = field.className + " error";
			// pas d'envoi de formulaire
			form_submit = false;
		}
	});

	if (!form_submit && focused != null) {
		focused.focus();
	}

	// si false bloquer l'envoi du formulaire
	return form_submit;
}
<?php

require('fpdf/fpdf.php');

$quizz = new SimpleXmlElement('metadata/quizz.xml', null, true);

$output = array();

if (!empty($_POST['submit'])) {
	$submit = true;
	unset($_POST['submit']);
	$nb_good_answers = 0;

	$pdf = new FPDF();
	$pdf->addPage();
	$pdf->setFont('Helvetica', 'B', 16);
	$pdf->cell(0, 10, utf8_decode('Résultats du quizz'), 0, 2, 'C');
	$pdf->cell(0, 8, '', 0, 1);

	foreach ($_POST as $inc_id_question => $inc_id_answer) {
		// to fix the incrementation done in html part 
		$id_question 	= $inc_id_question - 1;
		$id_answer 		= $inc_id_answer - 1;

		# decomposition of xml structure
		$tour = $quizz->tour[$id_question];
		$question = $tour->question;
		# set font for question part
		$pdf->cell(0, 3, '', 0, 1);
		$pdf->setFont('Helvetica', 'U', 10);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->cell(200, 10, utf8_decode('Question: ' . $question), 0, 2, 'L');
		$pdf->cell(0, 1, '', 0, 1);

		$pdf->setFont('Helvetica', '', 10);
		$reponse_donnee = $tour->reponse[(int) $id_answer];
		$pdf->cell(20);
		$pdf->cell(200, 10, utf8_decode('réponse donnée: ' . $reponse_donnee), 0, 2, 'L');

		$status = $reponse_donnee->attributes()->status;

		if ($status == "vrai") {
			$nb_good_answers++;
			$output[$inc_id_question] = '<span class="valid">Félicitations, la réponse était bien: ' . $reponse_donnee . '</span>';

			$pdf->cell(200, 10, utf8_decode('réponse correcte: ' . $reponse_donnee), 0, 2, 'L');
			$pdf->SetTextColor(0, 139, 0);
			$pdf->cell(200, 10, utf8_decode('1 point'), 0, 2, 'L');
			$pdf->cell(-20);
		} else {
			$i = 0;
			$found = false;
			
			while ($found == false) {
				$reponse_correcte = $tour->reponse[(int) $i];

				if ($reponse_correcte->attributes()->status == "vrai") {
					$found = true;
				}

				$i++;
			}

			$output[$inc_id_question] = '<span class="error">Erreur, la réponse était: ' . $reponse_correcte . '</span>';

			$pdf->cell(200, 10, utf8_decode('réponse correcte: ' . $reponse_correcte), 0, 2, 'L');
			$pdf->SetTextColor(205, 38, 38);
			$pdf->cell(200, 10, utf8_decode('0 point'), 0, 2, 'L');
			$pdf->cell(-20);
		}
	}

	$pdf->cell(0, 10, '', 0, 1);
	$pdf->setFont('Helvetica', '', 14);
	$pdf->SetTextColor(0, 0, 0);
	$pdf->cell(200, 10, utf8_decode('Nombre total de points: ' . $nb_good_answers), 0, 2, 'C');

	$pdf->output();
}



echo '<!doctype html>';
echo '<head>';
if (true) {
	echo '<meta charset="utf-8">';
	echo '<title>un super quizz</title>';

	echo '<style type="text/css">';
	if (true) {
		echo 'body {font-family: "Trebuchet MS", Helvetica, sans-serif; font-size:1em; width:100%; margin:auto; max-width:965px;}';
		echo 'h1 {text-decoration:underline; margin-bottom:30px}';
		echo 'ul {margin-bottom:15px;}';
		echo 'li.question {font-size: 0.9em}';
		echo 'label.reponse {font-size: 0.8em}';
		echo 'span.valid {color:green}';
		echo 'span.error {color:red}';
	}
	echo '</style>';
}
echo '</head>';
echo '<body>';
if (true) {
	echo '<form action="quizz.php" method="post">';
	echo '<h1>A vous de jouer:</h1>';
	echo '<ul>';
	$num_question = 0;

	foreach ($quizz->tour as $num_tour => $tour) {
		echo '<li class="question">';
		echo $tour->question;
		echo '<ul>';
		$num_reponse = 0;
		++$num_question;

		foreach ($tour->reponse as $reponse) {
			++$num_reponse;
			$mapping = $num_question . $num_reponse;
			$checked = ($num_reponse == 1) ? ' checked' : '';

			echo '<div>';
			echo '<input type="radio" id="' . $mapping . '" name="' . $num_question . '" value="' . $num_reponse . '" ' . $checked . '>';
			echo '<label class="reponse" for="' . $mapping . '">' . $num_reponse . ': ' . $reponse . '</label>';
			echo '</div>';
		}

		echo '</ul>';
		if (isset($submit)) {
			echo $output[$num_question] . '</br></br>';
		}
		echo '</li>';
	}
	echo '<ul>';
	echo '<input type="submit" name="submit" value="envoyer"/>';
	echo "</form>"; 
}
echo '</body>';
echo ""; 
#!/usr/bin/gawk -f

# afficher les mots répétés dans un texte
# 
# mettre le texte en minuscule 
# retirer tous les caractères autre que a-z, 0-9, espace ou tab
# pour chaque mot $i trouvé, incrémenter sa fréquence tab_frequence[$i]++  


BEGIN {
	# define the shell size
	"stty -a | grep columns | tr -d ';' | cut -d' ' -f 7" | getline shellsize

	# display title
	print repeat_char("=", shellsize);
	print center_string("Bienvenue dans le M-A-G-N-F-I-Q-U-E parser de Guillaume Penaud", "=", "center");
	print repeat_char("=", shellsize);
	print "\n";
	s(0.7)
} $0 > NF {
	# minify sentence
	$0 = tolower($0)

	# delete ponctuation
	gsub(/[,.:;!?]+/, "")

	# array "frequences" with word in index and current frequence as value
	for (i = 1; i <= NF; i++) {    
		frequences[$i]++;
	}
} END {
	print center_string("Mise en minuscule", ".", "left")
	s(0.3)

	print center_string("Suppression des caractères inutiles", ".", "left")
	s(0.3)

	print center_string("Détection de la féquence des doublons", ".", "left")
	s(0.3)

	print center_string("Affichage des résultats", ".", "left")
	print "\n";
	s(0.3)

	for (word in frequences) {
		end_msg = "nb: " frequences[word] " "
		print center_string(word, ".", "left")
	}
}

function center_string(string, separator, type) {
	returned = string
	empty = shellsize - length(string)
	if (end_msg == "") {
		end_msg = "done !"
	}

	switch (type) {
		case "center":
		    if (empty > 0) {
		    	half = sprintf("%3.0f\n", (empty / 2)) - 1
		    	returned = repeat_char(separator, half+0) " " string " " repeat_char(separator, half+0) (empty % 2 == 0 ? "" : separator)
		    }
		    break
		case "left":
			returned = string repeat_char(separator, (empty - length(end_msg))) end_msg
			break
		default:
		    print "error"; exit 1;
	}

	return returned
}

function repeat_char(char, occurences) {
	token = "% "occurences"s";

	separator = sprintf(token, char);
	gsub(/ /, char, separator);

	return separator;
}

function s(time) {
	# print time
	system("sleep "time"")
}
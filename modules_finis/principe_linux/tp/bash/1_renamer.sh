#!/bin/sh

case $1 in
	"--generate" )
		mkdir files 2>/dev/null || exit 1

		for id in $(seq 0 100)
		do
			touch files/DCP_$id
		done
		;;
	"--destroy" )
		rm -rf files >/dev/null 2>&1
		;;
	*)
		for file in $*
		do
			echo "photo_${file#*_}";
		done
		;; 
esac


#!/bin/sh

file=$(ls -ld $1 2>/dev/null)
if [ $? -eq 2 ]; then echo "Oops! erreur, argument invalide"; exit 1; fi
user=$(echo $file | awk '{print $3}')

if [ -d $1 ]; then
  type_arg=répertoire
else 
  type_arg=fichier
fi

if [ -r $1 ]; then
  re=" lecture"
fi

if [ -w $1 ]; then
  w=" écriture"
fi

if [ -x $1 ]; then
  x=" exécution"
fi

echo Le fichier $1 est un $type_arg
echo "$1" est accessible par $user en $re $w $x

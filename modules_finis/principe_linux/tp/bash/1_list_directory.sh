#!/bin/sh

if [ "$1" = "--all" ]; then
  list=$(ls -a)
else 
  list=$(ls)
fi

current=$(pwd)

echo --------------- fichiers dans $current ---------------

for element in $list 
do
  if [ -f $element ]; then echo $element; fi
done

echo --------------- repertoires dans $current ----------------

for element in $list
do
  if [ -d $element ]; then echo $element; fi
done

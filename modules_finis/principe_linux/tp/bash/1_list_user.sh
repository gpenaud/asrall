#!/bin/sh

cat /etc/passwd | while read line; do
	uid=$(echo $line | cut -d: -f 3)

	if [ $uid -ge 500 ]; then
		echo $line | cut -d: -f 1
	fi
done
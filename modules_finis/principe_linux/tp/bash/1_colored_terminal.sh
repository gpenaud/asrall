#!/bin/bash
file="colors.txt"

nb_lines=`wc -l $file | cut -d' ' -f 1`
randomed=$(($RANDOM % $nb_lines))
i=0

cat $file | while read color; do
	if [ $i -eq $randomed ]; then
		xterm -bg $color
	fi

	i=$(($i+1))
done


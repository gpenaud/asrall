#!/bin/sh

directory=`ls`

for file in $directory
do
  file $file | grep -oP 'ASCII\stext(?!(\sexecutable))' >/dev/null

  if [ $? -eq 0 ]; then
    read -p "Voulez vous lire le fichier $file ? y/n: " yn
    case $yn in
        [Yy]* ) more $file;;
        [Nn]* ) echo "Passage au fichier suivant...";;
        * ) echo "Les réponses autorisées: y or n.";;
    esac
  fi
done
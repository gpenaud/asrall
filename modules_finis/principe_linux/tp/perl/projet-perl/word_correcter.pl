#!/usr/bin/perl

# force to adopt good habits
use warnings;
use strict;

# display title
print "\n";
print "###############################################\n";
print "####   Welcome into my amazing correcter   ####\n";
print "###############################################\n";
print "\n";


sub get_nb_chars_diffs {
    my $stop_search = 0;
    # the word to correct
    my ($word_to_correct) = @_;

    my @similar_words = ();

    # open the dictionnary only once (11M file)
    open my($dictionnary), '<', 'lib_word_correcter/dico.txt' or die "unable to open the dictionnary !\n";

    # split word to be compared with the dico one
    $word_to_correct =~ lc $word_to_correct;
    my @chars_to_correct = split("", $word_to_correct);

    # iterate while ther is still word
    while(my $word = <$dictionnary> and !$stop_search) {
        # keep only the word (first part of each line in the dictionnary)
        $word =~ s/(\s.*)+/\n/;

        # split the current word to be compared with the text word one
        my @chars = split("", $word);

        # useless to pursue the iterations after the first letter 
        if ($chars[0] gt $chars_to_correct[0]) {
            # PROBLEME: la comparaison de à et g donne à greater than g
            print $chars[0] . " --- " . $chars_to_correct[0];
            $stop_search = 1;
        }

        my $i = 0;
        my $nb_diff = 0;
        my $has_too_much_diff = 0;

        while (($i < @chars_to_correct) and !$has_too_much_diff) {
            if (
                defined($chars[$i])
                and ($chars_to_correct[$i] ne $chars[$i])
            ) {
                $nb_diff++;

                if ($nb_diff > 2) {
                    $has_too_much_diff = 1;
                }
            }

            $i++;
        }

        if (!$has_too_much_diff) {
            # push @similar_words, $word;
            # print @similar_words; 
        }
    }

    # close the file and remove the handler
    close($dictionnary);

    return 2;
}

# open the text to correct !
open my($text_to_correct), '<', 'lib_word_correcter/text_to_correct.txt' or die "unable to open the text to correct !\n";

# iterate while ther is still a word
while( my $line = <$text_to_correct> ) {
    $line =~ s/[,\.;:?!"]//g;
    $line =~ s/\n+//g;
    $line = lc $line;

    my @words = split(/ /, $line);
    
    foreach my $word (@words) {
        get_nb_chars_diffs($word);
    }
}
#!/usr/bin/perl

# force to adopt good habits
use warnings;
use strict;

# title
print "Welcome into my perl loto ! Do you wanna be rich today ? \n";

sub randomize {
    # initialize variables
    my $nb_parameters = @_;
    my ($min, $max) = @_;

     # check the number of parameters
    if ($nb_parameters != 2) {
        die("wrong number of arguments");
    }

    # define range from extremes
    my $range = $max - $min;

    # return result
    return int(rand($range)) + $min;
}

# 
my @result;

for (my $i = 1; $i < 6; $i++) {
    my $randomed = randomize(1, 50);

    if (defined($result[$randomed])) {
        print "oops ! redondance: $randomed - on relance !\n";
        $i--;
    } else {
        print "numéro $i: $randomed\n";
        $result[$randomed] = "true";
    }
}

for (my $i = 1; $i < 3; $i++) {
    my $randomed = randomize(1, 11);

    if (defined($result[$randomed])) {
        print "oops ! redondance: $randomed - on relance !\n";
        $i--;
    } else {
        print "numéro complémentaire $i: $randomed\n";
        $result[$randomed] = "true";
    }
}
#!/usr/bin/perl

# force to adopt good habits
use warnings;
use strict;
use Cwd;
use 5.010;

# display title
print "\n";
print "##############################################\n";
print "####   Welcome into my folder displayer   ####\n";
print "##############################################\n";
print "\n";

# configure tree spacing
use constant SPACES => 4;

# function which display recursively a given directory
sub listfiletree {
    # the path to read
    my ($path) = @_;
    # know how much pipes to display by line
    state @branches;
    # know if current folder is the root one 
    state $is_root = 1;
    # indentation level
    state $level = 0;

    # open the directory
    opendir (my $descriptor, $path) or die ("unable to open directory $path");

    # read it and filter . and ..
    my @folder = grep { !/^\.\.?$/ } readdir($descriptor);

    # close the directory
    closedir($descriptor);

    if ($is_root) {
        # we must not touch to $path variable (required later)
        my $displayed_path = $path;

        # change "." to the real path
        if ($displayed_path eq ".") {
            $displayed_path = cwd();
        }

        # clean directory part and every "/"
        $displayed_path =~ s/(\/)$//;
        $displayed_path =~ s/(.*\/)//;

        print " $displayed_path\n";
        # the future pathes won't be root anymore
        $is_root = 0;
    }

    # we're on a new level of the tree at each call of the function
    $level++;

    # sort by ascendant alphabetic order
    my @sorted_folder = sort @folder;

    # we iterate on each file of the current reading-directory
    foreach my $file (@sorted_folder) {
        # we iterate on each level of the tree to know what to draw
        for (my $i = 0; $i < $level; $i++) {
            # if a branch has been set for that level, we draw a pipe "|"
            if ($branches[$i]) {
                print "|" . " " x (SPACES - 1);
            } else {
                print " " . " " x (SPACES - 1);
            }
        }
        
        # display a node of the tree
        print "\\_";
        print "$file\n";
        
        if ( -d "$path/$file") {
            # if the current entry is a directory and is not the last entry
            # of that array level, we'll draw a branch for this level
            if ($file ne $sorted_folder[-1]) {
                $branches[$level]++;
            }

            # call recursively the current method
            listfiletree("$path/$file");
        }
    }

    # at this step, we quit a level, so we decrement both values
    $level--;
    $branches[$level]--;
}

if (defined($ARGV[0])) {
    listfiletree($ARGV[0]);
} else {
    listfiletree(".");
}


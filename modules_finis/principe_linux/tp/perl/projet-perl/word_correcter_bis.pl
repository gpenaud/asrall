#!/usr/bin/perl

# force to adopt good habits
use warnings;
use strict;
use 5.010;

# display title
print "\n";
print "###############################################\n";
print "####   Welcome into my amazing correcter   ####\n";
print "###############################################\n";
print "\n";

# proximity required for the word to be returned
use constant DISTANCE_MAX       => 2; 
# debug constants
use constant DEBUG_CHAR         => 0;
use constant DEBUG_FOUND        => 0;
use constant DEBUG_MATRICE      => 0;
use constant DEBUG_DISTANCE     => 0;
use constant DEBUG_CLOSE_WORDS  => 1;
use constant DEBUG_STOP         => 0;

# retourne la distance de Levenstein
sub get_close_words {
    # the word to correct
    my ($word_to_correct) = @_;
    # array of words returned by the function
    my @close_words = ();
    # the distance between the two words
    my $distance;
    # 
    my $found = 0;

    # open the dictionnary only once (11M file)
    open my($dictionnary), '<', 'lib_word_correcter/dico.txt' or die "unable to open the dictionnary !\n";

    # load all file in an array to grep for the word
    chomp(my @lines = <$dictionnary>);

    if (grep(/^$word_to_correct/, @lines)) {
        $found = 1;
    }

    # set word in lower case
    $word_to_correct =~ lc $word_to_correct;

    if (DEBUG_FOUND) {
        print $word_to_correct;
        print " - ";
        print $found;
        print "\n";
    }
    
    # rewind file
    seek $dictionnary, 0, 0;

    while (defined(my $word = <$dictionnary>) and !$found) {
        chomp($word);
        $word =~ s/\s.*//;

        # get the levenstein distance between the two words
        $distance = get_levenstein_distance($word_to_correct, $word);

        # if the distance is small enough, we shall propose the word as a correction
        if ($distance <= DISTANCE_MAX) {
            push @close_words, $word . ":" . $distance;
        }
    }

    # close the file and remove the handler
    close($dictionnary);

    return @close_words;
}

sub get_levenstein_distance {
    # the words from which distances will be calculated
    my ($word_to_correct, $word_from_dictionnary) = @_;
    # the distance between the two words
    my $distance = 0;
    # longueur des chaïnes
    my $l_word_to_correct        = length($word_to_correct);
    my $l_word_from_dictionnary  = length($word_from_dictionnary);
    # index des tableaux
    my $i = 0;
    my $j = 0;
    # initialisation de la variable de coût
    my $cost = 0;
    # initialisation de la matrice 
    my @matrice = ();
    # if min is greater than limit 
    my $broken = 0;

    # invert comparison if length problem
    if ($l_word_to_correct > $l_word_from_dictionnary) {
        # invert length value
        my $tmp = $l_word_from_dictionnary;
        $l_word_from_dictionnary = $l_word_to_correct;
        $l_word_to_correct = $tmp;

        # invert words
        $tmp = $word_from_dictionnary;
        $word_from_dictionnary = $word_to_correct;
        $word_to_correct = $tmp;
    }

    # initialisation de la partie verticale de la matrice
    for ($i = 0; $i <= $l_word_to_correct; $i++) {
        $matrice[$i][0] = $i;
    }

    # initialisation de la partie horizontale de la matrice
    for ($j = 0; $j <= $l_word_from_dictionnary; $j++) {
        $matrice[0][$j] = $j;
    }

    # import minimum library
    use List::MoreUtils qw( minmax );

    my $stop = 0;

    # remplissage de la matrice
    $i = 1;

    while ($i <= $l_word_to_correct and !$stop) {
        $j = 1;

        while ($j <= $l_word_from_dictionnary and !$stop) {
            # comparaison des lettres à l'index courant ($i)
            my $char_to_correct         = substr($word_to_correct, ($i - 1), 1);
            my $char_from_dictionnary   = substr($word_from_dictionnary, ($j - 1), 1);

            # DEBUG
            if (DEBUG_CHAR) {
                print $char_to_correct;
                print " - ";
                print $char_from_dictionnary;
                print "\n";
            }

            if ($char_to_correct eq $char_from_dictionnary) {
                $cost = 0;      
            } else {
                $cost = 1;
            }

            # set costs as list
            my @costs = (
                ($matrice[$i - 1][$j] + 1),         # deletion cost
                ($matrice[$i][$j - 1] + 1),         # insertion cost
                ($matrice[$i - 1][$j - 1] + $cost)  # substitution cost
            );

            # minimum cost 
            my ($minimum, $maximum) = minmax @costs;

            # set the current minimum value
            $matrice[$i][$j] = $minimum;

            # DEBUG
            if (DEBUG_MATRICE) {
                print $matrice[$i][$j] . "  ";
            }

            $j++;
        }

        # minimum value of the matrice's line 
        my ($minimum_line, $maximum_line) = minmax @{$matrice[$i]};

        # stop if the minimum of line is over the 
        if ($minimum_line > DISTANCE_MAX) {
            $stop = 1;

            if (DEBUG_STOP) {
                print "\n";
                print "minimum: ";
                print " - ";
                print $minimum_line;
                print "\n";
            }
        }

        $i++;

        # DEBUG
        if (DEBUG_MATRICE) {
            print "\n"; 
        }
    }

    if ($stop) {
        $distance = DISTANCE_MAX + 1;
    } else {
        $distance = $matrice[$l_word_to_correct][$l_word_from_dictionnary];    
    }
    
    # DEBUG
    if (DEBUG_DISTANCE) {
        print $word_to_correct;
        print " - ";
        print $word_from_dictionnary;
        print " - ";
        print $distance;
        print "\n";
    }

    return $distance;
}

# open the text to correct !
open my($text_to_correct), '<', 'lib_word_correcter/text_to_correct.txt' or die "unable to open the text to correct !\n";

# iterate while ther is still a word
while( <$text_to_correct> ) {
    my $line = lc;
    $line =~ s/([,\.;:?!"])//g;
    $line =~ s/(\n)//g;

    my @words = split(/ /, $line);
    
    foreach my $word (@words) {

        if (DEBUG_CLOSE_WORDS) {
            print $word;
            print ": (";
            print join(", ", get_close_words($word));
            print ")\n";
        } else {
            join(", ", get_close_words($word));
        }
    }
}
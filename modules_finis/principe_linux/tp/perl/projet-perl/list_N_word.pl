#!/usr/bin/perl

# force to adopt good habits
use warnings;
use strict;

# title
print "Welcome into the A-M-A-Z-I-N-G parser of me, The awesome Guillaume Penaud !\n";

# initialize the word's occurence hash
my %occurences;

# define the text file
my $text_file = "zola-utf8.txt";

# open file
open(FILE, $text_file) or die ("unable to open $text_file !\n");

# check first cli argument
if (!defined($ARGV[0]) or ($ARGV[0] !~ m/^\d+$/)) {
    die("the argument should be an integer");
}

# renaming for more consistency
my $min_occurence = $ARGV[0]; 

while (<FILE>) {
    $_ = lc $_;             # met en minuscule

    $_ =~ s/[,\.;:?!"]//g;  # supprime les ponctuations
    $_ =~ s/( \- )//g;      # supprime les tirets (de dialogues) 
    $_ =~ s/^\s*//;         # supprime les espaces de tête
    $_ =~ s/\s+/ /g;        # remplace les espaces multiples en une espace

    my @words = split(/ /, $_);

    foreach my $word (@words) {
        if (exists($occurences{$word})) {
            $occurences{$word}++;
        } else {
            $occurences{$word} = 1;
        }
    }
}

foreach my $word (sort {$occurences{$b} <=> $occurences{$a}} keys %occurences) {
    if ($occurences{$word} >= $min_occurence) {
        print "Le mot $word est présent $occurences{$word} fois\n";
    }
}
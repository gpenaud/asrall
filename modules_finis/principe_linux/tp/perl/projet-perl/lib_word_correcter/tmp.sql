INSERT INTO employees2
    (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, commission_pct, manager_id, department_id)
VALUES 
    (NULL, 'Guillaume', 'Penaud', 'GPENAUD', '666.222.475', '2008-10-13', 'SH_CLERK', 2600, NULL, 124, 50);


ALTER TABLE employees2 ALTER COLUMN last_name SET NOT NULL;
ALTER TABLE employees2 ALTER COLUMN first_name SET NOT NULL;
ALTER TABLE employees2 ALTER COLUMN job_id SET NOT NULL;

CREATE USER user1 WITH PASSWORD 'azerty';
CREATE USER user2 WITH PASSWORD 'azerty';
CREATE USER user3 WITH PASSWORD 'azerty';

SELECT
    * 
FROM
    pg_user pu,
    pg_group pg
WHERE
    pu.usesysid IN (pg.grolist)
    
INSERT INTO employeesU1 SELECT * FROM view_employees WHERE department_id IN (20, 50, 80);

CREATE TABLE emp_audit (
    quand     date,
    qui       varchar(10),
    quoi      varchar(10),
    emp_id    int,
    sal_old   int,
    sal_new   int
);


CREATE FUNCTION audit_on_employeesu1() RETURNS trigger AS $audit_employeesu1$
    BEGIN
        INSERT INTO emp_audit
        VALUES (
            (SELECT now()),
            (SELECT user),
            TG_OP,
            OLD.employee_id,
            OLD.salary,
            NEW.salary
        );
    END;
$audit_employeesu1$ LANGUAGE plpgsql;

    
CREATE RULE rule_audit_on_insert_employeesu1 AFTER INSERT OR DELETE OR UPDATE ON employeesu1
FOR EACH ROW EXECUTE PROCEDURE audit_on_employeesu1();

CREATE RULE "rule_audit_on_insert_employeesu1" AS ON INSERT TO employeesu1
    DO INSERT INTO emp_audit VALUES ((SELECT now()), (SELECT user), 'insert', NEW.employee_id, NULL, NEW.salary);

CREATE RULE "rule_audit_on_update_employeesu1" AS ON UPDATE TO employeesu1
    DO INSERT INTO emp_audit VALUES ((SELECT now()), (SELECT user), 'update', OLD.employee_id, OLD.salary, NEW.salary);

CREATE RULE "rule_audit_on_delete_employeesu1" AS ON DELETE TO employeesu1
    DO INSERT INTO emp_audit VALUES ((SELECT now()), (SELECT user), 'delete', OLD.employee_id, OLD.salary, NULL);
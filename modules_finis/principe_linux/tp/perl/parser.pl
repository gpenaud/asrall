#!/usr/bin/perl

# force to adopt good habits
use warnings;
use strict;

# the work to do:
#
# afficher les mots répétés dans un texte
# mettre le texte en minuscule 
# retirer tous les caractères autre que a-z, 0-9, espace ou tab
# pour chaque mot $i trouvé, incrémenter sa fréquence tab_frequence[$i]++  

print "Bienvenue dans le M-A-G-N-F-I-Q-U-E parser de Guillaume Penaud\n";

# initialize the word's occurence hash
my %occurences;

# define the text file
my $text_file = "zola-utf8.txt";

# open file
open(FILE, $text_file) or die ("Impossible d'ouvrir $text_file !\n");

while (<FILE>) {
    $_ = lc $_;             # met en minuscule

    $_ =~ s/[,\.;:?!"]//g;  # supprime les ponctuations
    $_ =~ s/( \- )//g;      # supprime les tirets (de dialogues) 
    $_ =~ s/^\s*//;         # supprime les espaces de tête
    $_ =~ s/\s+/ /g;        # remplace les espaces multiples en une espace

    my @words = split(/ /, $_);

    foreach my $word (@words) {
        if (exists($occurences{$word})) {
            $occurences{$word}++;
        } else {
            $occurences{$word} = 1;
        }
    }
}

while( my ($word,$occurence) = each(%occurences) ) {
   print "Le mot '$word' est présent $occurence fois\n";
}


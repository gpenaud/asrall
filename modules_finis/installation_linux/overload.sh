#!/bin/bash

load=$(cat /proc/loadavg | cut -d' ' -f 1)

if [ ${load%%.*} -gt 0 ]; then
    ps aufx
    top
fi
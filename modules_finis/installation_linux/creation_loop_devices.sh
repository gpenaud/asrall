#!/bin/sh

modprobe loop;

for i in $(seq 1 4); do
    dd if=/dev/zero of=/tmp/blockdev$i bs=1M count=100
    losetup /dev/loop$i /tmp/blockdev$i
done
